# CodeSegment

## 介绍
代码片段


## 结构

### iOS
#### OC
- Category知识点
- GCD计时器
- block知识点
- webview添加头尾视图
- 单例实现
- 扇形进度条
- 环形进度条



#### swift
- VPN检测
- 单例实现
- 手机网络类型
- 投巧式防截屏
- WCDB数据库



### Python
- tcp通信练习
- 区块链初识
- 学习排序
