# 服务端与客户端通信练习





Server.py

```python
#! /usr/bin/env python3

# coding=utf-8



from socket import *

from time import ctime



"""

host变量是空白的，这是对 bind()方法的标识，表示它可以使用任何可用的地址

port : 服务端端口

addr : 服务端地址

buf_size : 缓冲区大小（1KB）

"""

HOST = ''

PORT = 12345

ADDR = (HOST, PORT)

BUF_SIZE = 1024



'''

服务端socket执行的方法

ss.bind(addr) : 绑定主机地址

ss.listen() : 开始tcp socket监听 

'''

con_count = 5  # 传入连接请求最大数



tcp_server_sock = socket(AF_INET, SOCK_STREAM)  # 创建服务端的TCP socket

tcp_server_sock.bind(ADDR)

tcp_server_sock.listen(con_count)



while True:

	print('Wiatting connected...')

	tcp_client_sock, addr = tcp_server_sock.accept()

	print('Connecting client addr is',str(addr))

	while True:

		try:

			receive_data = tcp_client_sock.recv(BUF_SIZE)  # 收到客户端连接发送来的数据

			print(addr[0], str(receive_data.decode('utf-8')))

			if not receive_data:

				break

			string = '['+str(ctime())+'] '+ str(receive_data.decode('utf-8'))

			tcp_client_sock.send(string.encode('utf-8'))  # 发送数据给客户端连接

		except Exception as e:

			print(e)

			raise e

		

	tcp_client_sock.close()



tcp_server_sock.close()
```







Client.py

```python
#! /usr/bin/env python3

# coding=utf-8



from socket import *

from time import ctime



HOST = 'localhost'

PORT = 12345

ADDR = (HOST, PORT)

BUF_SIZE = 1024



tcp_client_sock = socket(AF_INET,SOCK_STREAM)  # 创建客户端socket

tcp_client_sock.connect_ex(ADDR)  # 连接服务端socket



while True:

	data = input('>')  # type : str

	if not data:  # 输入空时 退出循环 关闭客户端连接

		break

	tcp_client_sock.send(data.encode('utf-8'))  # 客户端发送数据给服务端

	recv_data = tcp_client_sock.recv(BUF_SIZE)  # 服务端返回的数据

	if not recv_data:

		break

	print(str(recv_data.decode('utf-8')))



tcp_client_sock.close()
```

