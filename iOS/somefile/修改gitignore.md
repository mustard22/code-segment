# 修改.gitignore文件

1. 在对应.git目录下，执行
git rm -r --cached .

2. 应用.gitignore等本地配置文件重新建立Git索引
git add .

3. 提交当前Git版本并备注说明
git commit -m 'update .gitignore'
