# Category知识点记录



```objective-c
#import <Foundation/Foundation.h>



@interface NSObject (Expand)

/// 为NSObject的Category添加属性Expand

@property (nonatomic, copy) NSString *Expand;



/// 获取类的所有成员变量名

+ (NSArray *)ivarListWithClass:(Class)className;



/// 获取类的所有属性名

+ (NSArray *)propertyListWithClass:(Class)className;



/// 获取类的所有方法名

+ (NSArray *)methodListWithClass:(Class)className;



/// 获取类的所有协议名

+ (NSArray *)protocolListWithClass:(Class)className;

@end
```





```objective-c
#import "NSObject+Expand.h"

#import <objc/runtime.h>



@implementation NSObject (Expand)

/// 获取类的所有成员变量名

+ (NSArray *)ivarListWithClass:(Class)className

{

    unsigned int count;

    Ivar *list = class_copyIvarList(className, &count);

    NSMutableArray *array = [NSMutableArray arrayWithCapacity:count];

    for (unsigned int i = 0; i<count; i++) {

        Ivar ivar = list[i];

        const char *name = ivar_getName(ivar);

        [array addObject:[NSString stringWithUTF8String:name]];

    }

    return array;

}



/// 获取类的所有属性名

+ (NSArray *)propertyListWithClass:(Class)className

{

    unsigned int count;

    objc_property_t *list = class_copyPropertyList(className, &count);

    NSMutableArray *array = [NSMutableArray arrayWithCapacity:count];

    for (unsigned int i = 0; i<count; i++) {

        const char *name = property_getName(list[i]);

        [array addObject:[NSString stringWithUTF8String:name]];

    }

    return array;

}



/// 获取类的所有方法名

+(NSArray *)methodListWithClass:(Class)className

{

    unsigned int count;

    Method *list = class_copyMethodList(className, &count);

    NSMutableArray *array = [NSMutableArray arrayWithCapacity:count];

    for (unsigned int i = 0; i<count; i++) {

        Method method = list[i];

        NSString *name = NSStringFromSelector(method_getName(method));

        [array addObject:name];

    }

    return array;

}



/// 获取类的所有协议名

+ (NSArray *)protocolListWithClass:(Class)className

{

    unsigned int count;

    // 协议列表

    __unsafe_unretained Protocol **list = class_copyProtocolList(className, &count);

    NSMutableArray *array = [NSMutableArray arrayWithCapacity:count];

    // 遍历列表拿到对应的字符串

    for (unsigned int i = 0; i<count; i++) {

        Protocol *p = list[i];

        const char *name = protocol_getName(p);

        [array addObject:[NSString stringWithUTF8String:name]];

    }

    return array;

}





#pragma mark - 实现属性关联

/**

 *  Category一般用来添加方法，不支持添加属性。若要实现添加属性，需要关联处理

 */

/// 获取关联

- (NSString *)Expand {

    return objc_getAssociatedObject(self, @selector(Expand));

}



/// 关联

- (void)setExpand:(NSString *)Expand {

    objc_setAssociatedObject(self, @selector(Expand), Expand, OBJC_ASSOCIATION_COPY_NONATOMIC);

}

@end
```

