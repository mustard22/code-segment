# WKWebview添加headview和footview



```objective-c
// MyWebViewController.h

#import <UIKit/UIKit.h>

/// 给WKWebView添加headView\footView
@interface MyWebViewController : UIViewController
@end



//  MyWebViewController.m

#import "MyWebViewController.h"

#import <WebKit/WKWebView.h>



@interface MyWebViewController ()

@property (nonatomic, strong) WKWebView *webView;

@property (nonatomic, strong) UIView *head;

@property (nonatomic, strong) UIView *foot;

@end



@implementation MyWebViewController



- (void)viewDidLoad {

    [super viewDidLoad];

    // Do any additional setup after loading the view.

    CGSize screenSize = [UIScreen mainScreen].bounds.size;

    

    _head = [UIView new];

    _head.frame = CGRectMake(0, -100, screenSize.width, 100);

    _head.backgroundColor = [UIColor purpleColor];

    

    _foot = [UIView new];

    _foot.frame = CGRectMake(0, 0, screenSize.width, 200);

    _foot.backgroundColor = [UIColor orangeColor];

    

    WKWebView *web = [WKWebView new];

    web.frame = self.view.bounds;

    [self.view addSubview:web];

    _webView = web;

    [self addWebScrollObserver];

    

    web.scrollView.contentInset = UIEdgeInsetsMake(_head.frame.size.height, 0, 0, 0);

    [web.scrollView addSubview:_head];

    

    

    // load

    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://github.com/"]];

    [web loadRequest:req];

}



- (void)dealloc {

    [self removeWebScrollObserver];

}



- (void)addWebScrollObserver {

    [self.webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];

}



- (void)removeWebScrollObserver {

    [self.webView.scrollView removeObserver:self forKeyPath:@"contentSize"];

}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {

    if ([keyPath isEqualToString:@"contentSize"]) {

        UIScrollView *scroll = self.webView.scrollView;

        for (UIView *view in scroll.subviews) {

            if ([view isKindOfClass:NSClassFromString(@"WKContentView")]) {

                // set foot

                scroll.contentInset = UIEdgeInsetsMake(_head.frame.size.height, 0, _foot.frame.size.height, 0);

                [scroll insertSubview:_foot belowSubview:view];

                

                CGRect rect = _foot.frame;

                rect.origin.y = scroll.contentSize.height;

                _foot.frame = rect;

            }

        }

    }

}





- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];

    // Dispose of any resources that can be recreated.

}



@end
```