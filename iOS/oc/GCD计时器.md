# GCD计时器实现



```objective-c
/* GCD实现计时器代码块 */



@property(nonatomic, strong) dispatch_source_t timer;



/// 设置计时器（用GCD实现，尽量减少误差）

- (void)setupTimer

{

    // 时长

    __block NSInteger count = 10;

    

    // 设置全局并发队列

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    

    // 创建timer(要设置为全局的，否则执行一次就退出)

    self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);



    // 开始时间

    dispatch_time_t start = dispatch_time(DISPATCH_TIME_NOW, 0);

    

    // 时间间隔

    NSTimeInterval ti = 1.0;

    

    // 设置timer

    dispatch_source_set_timer(self.timer, start, (int64_t)ti * NSEC_PER_SEC, 0);

    

    __weak typeof(self)wself = self;

    dispatch_source_set_event_handler(self.timer, ^{

        __strong typeof(wself)sself = wself;

        if (count > 0) {

            count--;

        } else {

            // 取消timer

            dispatch_source_cancel(sself.timer);

            sself.timer = nil;

        }

    });

    

    // 初始化的timer默认是挂起的 需要激活

    dispatch_resume(self.timer);

}
```

