# 扇形进度条



```objective-c
#import <UIKit/UIKit.h>

/// 练习 扇形进度条

@interface CHLoadView : UIView



// 加载进度

@property (nonatomic, assign) CGFloat progress;



+(CHLoadView*)showInView:(UIView *)view;



- (void)hide;

@end
```



```objective-c
#import "CHLoadView.h"

@interface CHLoadView()

@property (nonatomic, strong) UILabel *loadLabel;

@end

@implementation CHLoadView



// Only override drawRect: if you perform custom drawing.

// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect {

    /// 画一个环

    UIBezierPath *bpath = [UIBezierPath bezierPathWithRoundedRect:[self getRect] cornerRadius:[self getRadius]];

    [[UIColor orangeColor] set];

    bpath.lineWidth = 1.0;

    [bpath stroke];

    

    

    /// 根据进度画扇形

    // 开始位置

    CGFloat startAngle = -M_PI_2;

    CGFloat endAngle = startAngle + self.progress * M_PI * 2;

    // clockwise : 是否顺时针

    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:[self getCenter] radius:[self getRadius] startAngle:startAngle endAngle:endAngle clockwise:YES];

    // 形成闭合路线

    [path addLineToPoint:[self getCenter]];

    // 设置颜色

    [[UIColor orangeColor] set];

    // 填充

    [path fill];

}



- (CGPoint) getCenter {

    return self.center;

}



/// 半径

- (CGFloat) getRadius {

    return 50.0;

}



/// 圆显示的区域

- (CGRect) getRect {

    

    CGRect rect = {[self getCenter].x-[self getRadius], [self getCenter].y-[self getRadius], [self getRadius]*2, [self getRadius]*2};

    return rect;

}



/// 显示扇形进度条

+ (CHLoadView *)showInView:(UIView *)view

{

    CHLoadView *lv = [[CHLoadView alloc] initWithFrame:view.bounds];

    [view addSubview:lv];

    return lv;

}



- (instancetype)initWithFrame:(CGRect)frame {

    if (self = [super initWithFrame:frame]) {

        /// 百分比label

        CGFloat viewWidth = 35.0f;

        _loadLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, viewWidth, viewWidth)];

        _loadLabel.textAlignment = NSTextAlignmentCenter;

        _loadLabel.textColor = [UIColor whiteColor];

        _loadLabel.font = [UIFont systemFontOfSize:10.0f];

        _loadLabel.center = self.center;

        _loadLabel.text = @"0%";

        [self addSubview:_loadLabel];

    }

    return self;

}



/// 设置进度

- (void)setProgress:(CGFloat)progress {

    _progress = progress;

    dispatch_async(dispatch_get_main_queue(), ^{

        // 重绘

        [self setNeedsDisplay];

        // 进度为1 移除view

        if (progress == 1.0) {

            [self hide];

        }

        // 设置百分比label

        _loadLabel.text = [NSString stringWithFormat:@"%.0f%%",_progress*100.0f];

    });

}



/// 从父视图移除

- (void)hide {

    // 延时0.5秒移除

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

       [self removeFromSuperview];

    });

}

@end
```





<img src="/Users/mac/code/mycode/codesegment/iOS/oc/shoot2.png" alt="shoot2" style="zoom:50%;" />

