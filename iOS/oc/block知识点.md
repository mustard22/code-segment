# block块知识点记录



```objective-c
// MRC下使用block(默认是在stack上，大多操作需要retain || copy)



__block typeof(self)wself = self; // 这句说明两点：1.修饰的变量在block内可改变；2.对self不进行‘隐式retain’。

void(^myblock)(void) = ^{

    // 判断wself是否存在：

    // if(wself) {

        // 这种姿势有问题（当wself已经被释放了，在MRC下不自动指向nil，此时的wself是传说中的‘野指针’）

    // }



    // 正确姿势如下：

    if(malloc_zone_from_ptr(wself)) {

        // malloc_zone_from_ptr()方法是C库中的方法（要导入）

    }

};







// ARC下使用block(默认是在heap上)



__block typeof(self)wself = self; // 说明1点：变量在block块内是可改变的



__weak typeof(self)wself = self; // 生成1个self的若引用 避免循环使用问题

void (^myblock2)(void) = ^{

    if(wself) { // 在ARC下判断self是否存在正确姿势(ARC下释放变量后 指针会指向空->nil)

        __strong typeof(wself)sself = wself; // 生成wself的强引用(在栈上) 避免wself被释放； 出了block就释放了

        // do something with 'sself'(sself.name = @"")

    }

};
```