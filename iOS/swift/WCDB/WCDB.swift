//
//  WCDB.swift
//  CSIMApp
//
//  Created by mac on 2021/4/2.
//

import WCDBSwift

// -------------------test class----------------------

struct Conversation: TableCodable {
    
    var id: Int = 0
    
    /// 会话id(对应用户uid)
    var cid: String?
    /// 头像
    var avatar: String?
    /// 昵称
    var nick: String?
    
    var msgs: [Message]?
    
    /// 用于定义是否使用自增的方式插入
    var isAutoIncrement: Bool = true
    
    /// 用于获取自增插入后的主键值
    var lastInsertedRowID: Int64 = 0

    enum CodingKeys: String, CodingTableKey {
        typealias Root = Conversation
        static let objectRelationalMapping = TableBinding(CodingKeys.self)

        case id
        case cid
        case nick
        case avatar
        
        case msgs
        
        //Column constraints for primary key, unique, not null, default value and so on. It is optional.
        static var columnConstraintBindings: [CodingKeys: ColumnConstraintBinding]? {
            return [
                //自增主键的设置
                .id: ColumnConstraintBinding(isPrimary: true, isAutoIncrement: true)
            ]
        }

    }
}

struct Message: TableCodable {
    
    var id: Int = 0
    
    /// 消息id
    var mid: String?
    /// 会话id(对应用户uid)
    var cid: String?
    /// 头像
    var avatar: String?
    /// 昵称
    var nick: String?
    /// 时间戳
    var time: String?
    
    /// 消息类型(文本，图片，服务器)
    var type: String?
    /// 标记发送成功
    var sendSuccess: Bool = false
    /// 是否已读
    var isRead: Bool = false
    /// 是否接收的消息（消息方向）
    var isReceive: Bool = false
    
    /// 服务器消息类型（一般，音视频通话）
    var tipsType: String?
    /// 语音、视频通话(对方主动断开1，自己断开2，正常结束3)
    var callStatusType: String?
    /// 时长
    var duration: Int?
    /// 图片
    var url: String?
    /// 各种要显示的文本内容
    var content: String?
    /// 音频数据
    var data: Data?
    
    
    
    /// 用于定义是否使用自增的方式插入
    var isAutoIncrement: Bool = true
    
    /// 用于获取自增插入后的主键值
    var lastInsertedRowID: Int64 = 0

    enum CodingKeys: String, CodingTableKey {
        typealias Root = Message
        static let objectRelationalMapping = TableBinding(CodingKeys.self)

        case id
        case mid
        case cid
        case avatar
        case nick
        case time
        case type
        case sendSuccess
        case isRead
        case isReceive
        case tipsType
        case callStatusType
        case duration
        case url
        case content
        case data
        
        //Column constraints for primary key, unique, not null, default value and so on. It is optional.
        static var columnConstraintBindings: [CodingKeys: ColumnConstraintBinding]? {
            return [
                //自增主键的设置
                .id: ColumnConstraintBinding(isPrimary: true, isAutoIncrement: true)
            ]
        }

    }
}



// -----------------------------------------

enum WCDBTableName : String, CaseIterable {
    /// 会话表
    case conversation = "conversation"
//    /// 消息表
//    case message = "message"
//    /// 未读消息
//    case unread = "unreadTable"
}

class WCDB: NSObject {
    
    static let share = WCDB()
    
    var db: Database?
    
    private let dbRootPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first! + "/DB/"
    private var dbName = ""
    
    // 数据库路径
    public var dbPath: String {
        return dbRootPath + dbName
    }
}


//MARK: private methods
extension WCDB {
    private func initial(with uid: String) {
        dbName = "wcdb-\(uid).db"
        db = createDB()
        
        for t in WCDBTableName.allCases {
            initalTable(t)
        }
    }
    
    // 创建数据库
    private func createDB() -> Database {
        return Database(withPath: self.dbPath)
    }
    
    /// 数据库与表的初始化
    private func initalTable(_ tableName: WCDBTableName) {
        do {
            // 1. 创建主数据库main的相关表
            switch tableName {
            case .conversation:
                try db?.run(transaction: {
                    self.createTable(table: tableName, modelType: Conversation.self)
                 })
            }
         
        } catch let error {
            debugPrint("初始化数据库及ORM对应关系建立失败\(error.localizedDescription)")
        }
    }

    /// 创建表
    private func createTable<T: TableDecodable>(table: WCDBTableName, modelType: T.Type) {
        do {
            try db?.create(table: table.rawValue, of: modelType)
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
}


//MARK: public methods
extension WCDB {
    // 登录成功后 初始化
    public static func initialize(with uid: String) {
        debugPrint("\n---------------------WCDB initial----------------------")
        guard let _ = Int(uid) else {
            debugPrint("wcdb初始化失败 uid error: <\(uid)>")
            return
        }
        
        share.initial(with: uid)
        
        debugPrint("-------------------------end----------------------------\n")
    }
    
    /// 插入数据
    public func insert<T: TableEncodable>(objects:[T], intoTable table: WCDBTableName){
        do {
            try db?.insert(objects: objects, intoTable: table.rawValue)
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }

    /// 修改
    public func update<T: TableEncodable>(fromTable table: WCDBTableName, on propertys:[PropertyConvertible], itemModel object:T,where condition: Condition? = nil){
        do {
            try db?.update(table: table.rawValue, on: propertys, with: object, where: condition)
        } catch let error {
            debugPrint(" update obj error \(error.localizedDescription)")
        }
    }

    /// 删除
    public func deleteFromDb(fromTable table: WCDBTableName, where condition: Condition? = nil) {
        do {
            try db?.delete(fromTable: table.rawValue, where:condition)
        } catch let error {
            debugPrint("delete error \(error.localizedDescription)")
        }
    }

    /// 查询
    public func qurey<T: TableDecodable>(fromTable table: WCDBTableName, where condition: Condition? = nil, orderBy orderList:[OrderBy]? = nil) -> [T] {
        guard let db = db else {
            return []
        }
        
        do {
            let items: [T] = try db.getObjects(fromTable: table.rawValue, where: condition, orderBy: orderList)
            return items
        } catch let error {
            debugPrint("no data find \(error.localizedDescription)")
        }
        return []
    }
    
    /// 删除数据表
    func dropTable(table: WCDBTableName) -> Void {
        do {
            try db?.drop(table: table.rawValue)
        } catch let error {
            debugPrint("drop table error \(error)")
        }
    }
    
    /// 删除所有与该数据库相关的文件
    func removeDbFile() -> Void {
        do {
            try db?.close(onClosed: {
                try db?.removeFiles()
            })
        } catch let error {
            debugPrint("not close db \(error)")
        }
        
    }
    
    /// 删除沙盒存储的数据库文件
    func deleteDataBaseFile() {
        let fmanager = FileManager.default
        
        guard fmanager.fileExists(atPath: dbRootPath), let paths = try? fmanager.contentsOfDirectory(atPath: dbRootPath) else {
            return
        }
        
        let delItems = paths.filter({$0.contains(dbName)})
        guard !delItems.isEmpty else {
            return
        }
        
        _ = delItems.map { (p) in
            let del = dbRootPath + p
            try? fmanager.removeItem(atPath: del)
        }
    }
}
