```swift
import Foundation

// print
public func CLog(_ item: @autoclosure()->Any, _ file: StaticString = #fileID, _ line: UInt = #line, _ fun: StaticString = #function) {
    print("\n\(Date()) | \(file):\(line) | func \(fun)\n\(item())\n")
}


```
