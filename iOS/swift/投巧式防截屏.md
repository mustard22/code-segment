# 视频或录屏内容防截屏



从这里得到解决方法

https://www.jianshu.com/p/857006f27dd2?ivk_sa=1024320u



```swift

/// 使用

/// let view = ForbidScreenShootView()

/// view.contentView = yourVideoView

/// 



/// 实现

class ForbidScreenShootView: UIView {

    private lazy var backView = _backView

    private lazy var topBtn = _topBtn

    

    /// 承载视频或录屏view

    open var contentView: UIView? {

        willSet {

            guard let v = newValue else {

                return

            }



            v.autoresizingMask = .all



            guard let pview = backView.subviews.first else {

                /// 有可能在iOS13以下版本，会走这里

                v.frame = backView.bounds

                backView.addSubview(v)

                return

            }

            

            pview.autoresizingMask = .all

            pview.frame = backView.bounds

            

            v.frame = pview.bounds

            pview.addSubview(v)

        }

    }

    

    /// 添加点击事件

    open var tapHandler: (()->Void)? {

        willSet {

            if let _ = newValue {

                topBtn.isEnabled = true

            } else {

                topBtn.isEnabled = false

            }

        }

    }

}




private extension ForbidScreenShootView {

    var _backView: UITextField {

        let v = UITextField()

        v.isSecureTextEntry = true

        v.frame = self.bounds

        v.isEnabled = false

        v.autoresizingMask = .all

        insertSubview(v, at: 0)

        return v

    }

    

    var _topBtn: UIButton {

        let v = UIButton()

        v.frame = self.bounds

        v.autoresizingMask = .all

        v.addTarget(self, action: #selector(tapAction), for: .touchUpInside)

        addSubview(v)

        return v

    }

    

    

    @objc func tapAction() {

        tapHandler?()

    }

}





public extension UIView.AutoresizingMask {

    static var all: Self {

        return [.flexibleHeight, .flexibleWidth, .flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]

    }

}
```

