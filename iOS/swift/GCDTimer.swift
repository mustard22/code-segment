//
//  GCDTimer.swift
//  RXSwiftApp
//
//  Created by walker on 2022/10/21.
//

import Foundation

public struct GCDTimer {
    private var timer: DispatchSourceTimer?
    private let queue = DispatchQueue.init(label: "com.gcd.timer", attributes: .concurrent)
}


public extension GCDTimer {
    /// 开始timer
    mutating func start(timeInterval: TimeInterval, repeats: Bool, handle: @escaping ()->Void) {
        cancle()
        
        let deadline = DispatchTime.now() + (repeats ? 0 : timeInterval)
        
        timer = DispatchSource.makeTimerSource(flags: [], queue: queue)
        timer?.schedule(deadline: deadline, repeating: repeats ? timeInterval : .infinity)
        timer?.setEventHandler(handler: handle)
        timer?.resume()
    }
    
    /// timer结束
    mutating func cancle() {
        timer?.cancel()
        timer = nil
    }
}

