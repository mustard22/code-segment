


# DrawWithPencilKit

## About
- 使用系统的PencilKit框架进行绘图（如备忘录的编辑功能）
- 参考官方：
https://developer.apple.com/documentation/pencilkit/drawing_with_pencilkit/

## 知识点
- undoManager的使用
- undo\redo\clear等操作实现

## 注意：只能在真机测试。模拟器上绘画不显示。

