//
//  PencilDrawView.swift
//  DrawApp
//
//  Created by walker on 2023/3/13.
//

import UIKit
import PencilKit

class PencilDrawView: UIView {
    fileprivate var canvasView: PKCanvasView!
    
    fileprivate var toolPicker: PKToolPicker!
    
    fileprivate var hasDrawing: Bool?
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(size: CGSize) {
        self.init(frame: CGRect(origin: .zero, size: size))
        
        doInitial()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// 更新外部操作按钮状态
    open var updateButtonStatusClosure: (()->())?
    
    /// 能否撤销绘制
    /// 只有进行了绘制，才可以撤销
    var canUndo: Bool {undoManager?.canUndo == true && hasDrawing == true}
    
    /// 能否重新绘制
    /// 只有进行了撤销，才可以重绘
    var canRedo: Bool {undoManager?.canRedo == true && hasDrawing == true}
    
    /// 能否清空
    var canClear: Bool {canUndo || canRedo}
    
    /// 撤销绘制
    func undo() {
        undoManager?.undo()
    }
    
    /// 重新绘制
    func redo() {
        undoManager?.redo()
    }
    
    
    /// 清空绘画
    func clear() {
        canvasView.drawing = PKDrawing()
    }
    
    
    /// 提取图片
    func toImage(scale: CGFloat)->UIImage {
        return canvasView.drawing.image(from: canvasView.bounds, scale: scale)
    }
    
    
    /// 重新进入绘画页面时 需要成为第一响应者
    /// func viewWillAppear(...) {setFirstResponder(true)}
    /// func viewWillDisappear(...) {setFirstResponder(false)}
    func setFirstResponder(_ isOk: Bool) {
        if isOk {
            canvasView.becomeFirstResponder()
        } else {
            canvasView.resignFirstResponder()
        }
    }
    
}



extension PencilDrawView: PKCanvasViewDelegate {
    func canvasViewDrawingDidChange(_ canvasView: PKCanvasView) {
        guard let _ = hasDrawing else {return}
        
        hasDrawing = true
        
        updateButtonStatusClosure?()
    }
    
    func canvasViewDidFinishRendering(_ canvasView: PKCanvasView) {
        // 初始化或清空（设置PKDrawing）时都会走这里
        hasDrawing = false
        
        // 刷新外部状态
        updateButtonStatusClosure?()
    }
}



fileprivate extension PencilDrawView {
    func doInitial() {
        // set canvas
        canvasView = PKCanvasView(frame: bounds)
        if #available(iOS 14.0, *) {
            canvasView.drawingPolicy = .anyInput
        } else {
            canvasView.allowsFingerDrawing = true
        }
        canvasView.delegate     = self
        canvasView.drawing      = PKDrawing()
        canvasView.contentSize  = canvasView.bounds.size
        addSubview(canvasView)
        
        // set toolPicker
        var tp: PKToolPicker
        if #available(iOS 14, *) {
            tp = PKToolPicker()
        } else {
            var win: UIWindow?
            if UIApplication.shared.connectedScenes.isEmpty {
                win = UIApplication.shared.windows.first(where: {$0.isKeyWindow})
            } else {
                win = UIApplication.shared.connectedScenes.compactMap({$0 as? UIWindowScene}).first(where: {$0.isFirstResponder})?.windows.first(where: {$0.isKeyWindow})
            }
            tp = PKToolPicker.shared(for: win!)!
        }
        toolPicker = tp
        toolPicker.setVisible(true, forFirstResponder: canvasView)
        toolPicker.addObserver(canvasView)
        
        updateLayout(for: toolPicker)
        
        registerUndoAction(drawing: canvasView.drawing)
        
        canvasView.becomeFirstResponder()
    }
    
    func updateLayout(for toolPicker: PKToolPicker) {
        let obscuredFrame = toolPicker.frameObscured(in: self)
        
        if obscuredFrame.isNull {
            canvasView.contentInset = .zero
        }
        
        else {
            canvasView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bounds.maxY - obscuredFrame.minY, right: 0)
        }
        canvasView.scrollIndicatorInsets = canvasView.contentInset
    }
    
    func registerUndoAction(drawing: PKDrawing) {
        let old = canvasView.drawing
        undoManager?.registerUndo(withTarget: self, handler: { selfTarget in
            selfTarget.registerUndoAction(drawing: old)
        })

        canvasView.drawing = drawing
    }
}
