//
//  ViewController.swift
//  DrawApp
//
//  Created by walker on 2023/3/13.
//

import UIKit

class ViewController: UIViewController {
    lazy var drawView: PencilDrawView = {
        let v = PencilDrawView(size: CGSize(width: view.bounds.width, height: view.bounds.width))
        v.updateButtonStatusClosure = {[weak self] in
            self?.resetButtonStatus()
        }
        view.addSubview(v)
        return v
    }()
    
    lazy var undoBtn: UIButton = {
        let v = UIButton()
        v.isEnabled = false
        v.frame.size = CGSize(width: 80, height: 40)
        v.setTitle("Undo", for: .normal)
        v.setTitleColor(.blue, for: .normal)
        v.setTitleColor(.lightGray, for: .disabled)
        v.addTarget(self, action: #selector(undoAction), for: .touchUpInside)
        view.addSubview(v)
        return v
    }()
    
    lazy var redoBtn: UIButton = {
        let v = UIButton()
        v.isEnabled = false
        v.frame.size = CGSize(width: 80, height: 40)
        v.setTitleColor(.blue, for: .normal)
        v.setTitleColor(.lightGray, for: .disabled)
        v.setTitle("Redo", for: .normal)
        v.addTarget(self, action: #selector(redoAction), for: .touchUpInside)
        view.addSubview(v)
        return v
    }()
    
    lazy var cleanBtn: UIButton = {
        let v = UIButton()
        v.isEnabled = false
        v.frame.size = CGSize(width: 80, height: 40)
        v.setTitleColor(.blue, for: .normal)
        v.setTitleColor(.lightGray, for: .disabled)
        v.setTitle("Clear", for: .normal)
        v.addTarget(self, action: #selector(clearAction), for: .touchUpInside)
        view.addSubview(v)
        return v
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawView.frame.origin = CGPoint(x: 0, y: 200)
        
        let space: CGFloat = (view.bounds.width - undoBtn.bounds.width*3)/3
        undoBtn.frame.origin = CGPoint(x: space, y: 100)
        redoBtn.frame.origin = CGPoint(x: undoBtn.frame.origin.x + undoBtn.frame.width + space, y: undoBtn.frame.origin.y)
        cleanBtn.frame.origin = CGPoint(x: redoBtn.frame.origin.x + redoBtn.frame.width + space, y: redoBtn.frame.origin.y)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        drawView.setFirstResponder(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        drawView.setFirstResponder(false)
    }
    
    func resetButtonStatus() {
        undoBtn.isEnabled = drawView.canUndo
        redoBtn.isEnabled = drawView.canRedo
        
        cleanBtn.isEnabled = drawView.canClear
    }
    
    @objc func clearAction() {
        drawView.clear()
    }
    
    @objc func undoAction() {
        drawView.undo()
    }
    
    @objc func redoAction() {
        drawView.redo()
    }
}
