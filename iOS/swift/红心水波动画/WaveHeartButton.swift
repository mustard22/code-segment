//
//  WaveHeartButton.swift
//  TestWave
//
//  Created by mac on 2022/7/8.
//

import UIKit

// 红心按钮
class WaveHeartButton: UIButton {
    private lazy var waveView: WaterAnimation = {
        let v = WaterAnimation()
        v.fillColor = .red
        v.waveColor = .red
        v.maskImage = UIImage(named: "img_heart_empty")
        addSubview(v)
        return v
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        waveView.frame = self.bounds
    }
    
    func setupProgress(value: Double) {
        waveView.setProgress(value: value) {[weak self] in
            if value >= 1 {
                self?.stopWaveAnimate()
            }
        }
    }
    
    private func stopWaveAnimate() {
        waveView.stopAnimation()
    }
}
