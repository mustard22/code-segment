//
//  WaterAnimation.swift
//  HiyChat
//
//  Created by mac on 2022/6/14.
//

/// 使用
/// DispatchQueue.main.asyncAfter(deadline: .now()+5) {

//let waterView = WaterAnimation()
//waterView.config.firstWaveColor = .red
//waterView.backgroundColor = .init(white: 1, alpha: 0.5)
//
//waterView.frame.size = CGSize(width: 100, height: 100)
//waterView.center = self.view.center
//self.view.addSubview(waterView)
//
//waterView.startAnimation()
//
//DispatchQueue.main.asyncAfter(deadline: .now()+5) {
//    waterView.setProgress(value: 0.8)
//}
//}
///


/// 水波浪动画
import UIKit

struct WaterAnimationConfig {
    /// 进度 0-1
    var progress    : Double = 0
    /// 波动速度，默认 1
    var speed       : Double = 1.0
    /// 波动幅度，默认 5
    var waveHeight  : Double = 5
    /// 是否显示单层波浪
    var isSingleWave: Bool = false
    
    // 波纹填充颜色
    var firstWaveColor: UIColor = .clear
    var secondWaveColor: UIColor = .clear
    
    var timer: CADisplayLink?
    
    var timer0: Timer?
}



/// 水波动画
class WaterAnimation: UIView {
    typealias ProgressCompletion = ()->Void
    
    open var config = WaterAnimationConfig()
    
    private var yHeight: Double = 0
    private var offset: Double = 0
    private var offsetProgress: Double = -1
    
    private lazy var firstWaveLayer = _waveLayer
    private lazy var secondWaveLayer = _waveLayer
    private lazy var topMaskLayer   = _topMaskLayer
    
    open var progressCompletion: ProgressCompletion?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        firstWaveLayer.frame = self.bounds
        if !config.isSingleWave {
            secondWaveLayer.frame = self.bounds
        }
        
        topMaskLayer.frame = self.bounds
    }
    
    deinit {
        stopTimer0()
        stopTimer()
    }
    
}


extension WaterAnimation {
    open var fillColor: UIColor {
        get {config.firstWaveColor}
        set {
            config.firstWaveColor = newValue
        }
    }
    
    open var waveColor: UIColor? {
        get {nil}
        set {
            guard let color = newValue else {
                return
            }
            
            config.secondWaveColor = color
        }
    }
    
    open var maskImage: UIImage? {
        get{nil}
        set {
            topMaskLayer.contents = newValue?.cgImage
        }
    }
    
    
    open func startAnimation() {
        if config.progress < 0 {
            config.progress = 0
        } else if config.progress > 1 {
            config.progress = 1
        }
        
        self.yHeight = self.bounds.height * (1.0 - config.progress)
        
        startTimer()
    }
    
    open func stopAnimation() {
        stopTimer()
    }
    
    open func setProgress(value: Double, completion: ProgressCompletion? = nil) {
        self.progressCompletion = completion
        
        offsetProgress = abs(value - config.progress)
        
        startTimer0()
    }
}


private extension WaterAnimation {
    
    func startTimer0() {
        stopTimer0()
        
        self.config.timer0 = Timer.init(timeInterval: 0.1, repeats: true, block: {[weak self] _ in
            self?.timerHandle0()
        })
        RunLoop.current.add(self.config.timer0!, forMode: .common)
    }
    
    func stopTimer0() {
        config.timer0?.invalidate()
        config.timer0 = nil
    }
    
    @objc func timerHandle0() {
        if offsetProgress <= 0 {
            stopTimer0()
            DispatchQueue.main.async {
                self.progressCompletion?()
            }
            return
        }
        
        offsetProgress -= 0.01
        config.progress += 0.01
        startAnimation()
    }
    
    
    func startTimer() {
        stopTimer()
        
        self.config.timer = CADisplayLink(target: self, selector: #selector(timerHandle))
        self.config.timer?.add(to: .current, forMode: .common)
    }
    
    func stopTimer() {
        config.timer?.invalidate()
        config.timer = nil
    }
    
    @objc func timerHandle() {
        
        setupLayerAnimation(layer: self.firstWaveLayer, fillColor: self.config.firstWaveColor)
        
        guard !config.isSingleWave else {
            return
        }
        
        setupLayerAnimation(layer: self.secondWaveLayer, fillColor: self.config.secondWaveColor)
    }
    
    func setupLayerAnimation(layer: CAShapeLayer, fillColor: UIColor) {
        var waveHeight = config.waveHeight
        if config.progress == 0.0 || config.progress == 1.0 {
            waveHeight = 0.0
        }
        
        self.offset += self.config.speed
        
        let doubleWidth: Double = Double(self.bounds.width)
        
        let path1 = CGMutablePath()
        let startY = waveHeight * sin(self.offset * .pi * 2 / doubleWidth)
        path1.move(to: .init(x: 0.0, y: startY))
        
        var originY: Double = 0
        
        for i in 0...Int(doubleWidth) {
            let di = Double(i)
            originY = waveHeight * sin((di + self.offset) * .pi * 2 / doubleWidth) + self.yHeight
            
            path1.addLine(to: .init(x: di, y: originY))
        }
        
        path1.addLine(to: .init(x: doubleWidth, y: originY))
        path1.addLine(to: .init(x: doubleWidth, y: self.bounds.height))
        path1.addLine(to: .init(x: 0, y: self.bounds.height))
        path1.addLine(to: .init(x: 0.0, y: startY))
        path1.closeSubpath()
        
        layer.path = path1
        layer.fillColor = fillColor.cgColor
    }
}



private extension WaterAnimation {
    var _waveLayer: CAShapeLayer {
        let l = CAShapeLayer()
        l.frame = self.bounds
        self.layer.addSublayer(l)
        return l
    }
    
    var _topMaskLayer: CALayer {
        let l = CALayer()
        l.frame = self.bounds
        layer.mask = l
        return l
    }
}
