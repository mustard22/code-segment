# VPN检测



```swift
struct VPN {
 2     /// 判断VPN是否打开
 3     static var isOpen: Bool {
 4         guard let cfDict = CFNetworkCopySystemProxySettings() else {
 5             return false
 6         }
 7         
 8         let nsDict = cfDict.takeRetainedValue() as NSDictionary
 9         
10         guard let keys = nsDict["__SCOPED__"] as? [String:Any] else {
11             return false
12         }
13         
14         let values: [String] = [
15             "tap",
16             "tun",
17             "ppp",
18             "ipsec",
19             "ipsec0",
20         ]
21         
22         var result = false
23         for key in keys.keys {
24             for (_,value) in values.enumerated() {
25                 if key.contains(value) {
26                     result = true
27                     break
28                 }
29             }
30             
31             if result {
32                 break
33             }
34         }
35         
36         return result
37     }
38 }
```

